<?php

/**
 * @file
 * Contains \Drupal\twig_routing\TwigRoutingServiceProvider.
 */

namespace Drupal\twig_routing;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Symfony\Component\DependencyInjection\Reference;

class TwigRoutingServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('twig')
      ->addMethodCall('addExtension', array(new Reference('twig_routing.twig_extension')));
  }

}
